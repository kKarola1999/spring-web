package edu.pwr.IOSpring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IoSpringApplication {

	public static void main(String[] args) {

		SpringApplication.run(IoSpringApplication.class, args);
		System.out.println("Hello World!");
	}

}
